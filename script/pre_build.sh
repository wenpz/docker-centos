#!/bin/bash

yum install -y sudo
yum install -y bzip2
yum install -y which
yum install -y make
yum install -y gcc
yum install -y gcc-c++
yum install -y cmake
yum install -y autoconf
yum install -y net-tools
yum install -y tree
yum install -y telnet
yum install -y ncurses-devel
