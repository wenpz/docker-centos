#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

mkdir -p $buildTmpRoot
cd $buildTmpRoot

tar jxvf $srcRoot/vim-7.4.tar.bz2
cd vim74
./configure --prefix=/usr/local/vim74 --enable-cscope --enable-multibyte --with-features=big
make
make install

cd $installDstRoot
tar zcvf vim74.tar.gz vim74

cp vim74.tar.gz $pkgRoot
