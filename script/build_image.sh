#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

cd $installDstRoot
tar zxvf $pkgRoot/vim74.tar.gz
ln -s vim74 vim

cd /usr/bin
mv vi vi.original
ln -s $installDstRoot/vim/bin/vim vim
ln -s vim vi

echo -e '\n'
echo -e 'alias vi="/usr/local/vim/bin/vim"\n' >> $HOME/.bashrc
echo -e 'alias vim="/usr/local/vim/bin/vim"\n' >> $HOME/.bashrc
echo -e 'alias vimdiff="/usr/local/vim/bin/vimdiff"\n' >> $HOME/.bashrc
echo -e '\n'
