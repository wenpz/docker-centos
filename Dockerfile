FROM scratch
MAINTAINER ligang <ligang1109@aliyun.com>

LABEL name="CentOS Image"
LABEL vendor="Andals"

ADD centos-7.2.1511-docker.tar.xz /

COPY pkg/ /build/pkg/
COPY script/ /build/script/

RUN /build/script/build_image.sh

CMD /build/script/init.sh
